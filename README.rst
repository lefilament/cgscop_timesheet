.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


==========================
CG SCOP - Feuille de temps
==========================

Ce module hérite du module Feuille de temps pour s'adapter aux spécifications des UR et de la CG.

Il ajoute les éléments suivants :

* menu pour configurer les projets (cf Codes Activité UR) et les vues associées
* table de configuration des activités CG
* table de configuration des dispositifs
* 


Credits
=======

Funders
------------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>
* Benjamin Rivier <benjamin@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
