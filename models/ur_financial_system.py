# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class ScopTimesheetCode(models.Model):
    _name = "ur.financial.system"
    _description = "Dispositif financier UR"

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    name = fields.Char('Nom')
    company_id = fields.Many2one(
        comodel_name='res.company',
        string='Société',
        default=lambda self: self.env.user.company_id)
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)
