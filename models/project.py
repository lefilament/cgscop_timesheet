# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class ScopProjectTimesheet(models.Model):
    _inherit = "project.project"

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    cgscop_timesheet_code_id = fields.Many2one(
        comodel_name='cgscop.timesheet.code',
        string='Code Activité National')
    privacy_visibility = fields.Selection(default="employees")
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)

    @api.onchange('name')
    def onchange_name(self):
        analytic = self.analytic_account_id
        if analytic and analytic.ensure_one():
            self.analytic_account_id.sudo().write({
                'name': self.name,
            })
