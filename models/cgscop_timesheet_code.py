# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class ScopTimesheetCode(models.Model):
    _name = "cgscop.timesheet.code"
    _description = "Code activité National"
    _order = 'name'

    name = fields.Char('Nom')
