{
    "name": "CG SCOP - Feuille de temps",
    "summary": "CG SCOP - Feuille de temps",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "hr_timesheet",
        "analytic",
        "project",
        "cgscop_partner",
    ],
    "data": [
        "security/ir.model.access.csv",
        "security/security_rules.xml",
        "views/assets.xml",
        "views/cgscop_timesheet_code.xml",
        "views/cgscop_timesheet_sheet.xml",
        "views/hr_timesheet.xml",
        "views/hr_timesheet_cgscop.xml",
        "views/res_partner.xml",
        "views/ur_financial_system.xml",
        "report/report_hr_timesheet.xml",
        "datas/cgscop_timesheet_code_data.xml",
    ]
}
